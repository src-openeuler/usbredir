Name:           usbredir
Version:        0.13.0
Release:        1
Summary:        network protocol libraries for sending USB device traffic
License:        LGPLv2+ and GPLv2+
URL:            https://www.spice-space.org/usbredir.html

Source0:        http://spice-space.org/download/%{name}/%{name}-%{version}.tar.xz


BuildRequires:  libusb1-devel >= 1.0.9 gcc
BuildRequires:  meson glib2-devel
Provides:       %{name}-server
Obsoletes:      %{name}-server

%description
usbredir is the name of a network protocol for sending USB device traffic over
a network connection. It is also the name of the software package offering a parsing
library, a usbredirhost library and several utilities implementing this protocol.
It also including a USB host TCP server, using libusbredirhost.

%package        devel
Summary:        Including header files and library for the developing of usbredir.
Requires:       usbredir = %{version}-%{release}

%description    devel
This contains dynamic libraries and header files for the developing of usbredir.


%package        help
Summary:        Including man files for usbredir
Requires:       man
BuildArch:      noarch

%description    help
This contains man files for the using of usbredir.


%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson
%meson_build


%install
%meson_install


%ldconfig_scriptlets


%files
%license COPYING COPYING.LIB
%{_libdir}/libusbredir*.so.*
%{_bindir}/usbredirect

%files devel
%doc ChangeLog.md
%{_includedir}/usbredir*.h
%{_libdir}/libusbredir*.so
%{_libdir}/pkgconfig/libusbredir*.pc

%files help
%{_mandir}/man1/usbredirect.1*


%changelog
* Tue Feb 7 2023 zhanghongtao <zhanghongtao22@huawei.com> - 0.13.0-1
- update to 0.13.0

* Tue Nov 1 2022 lihaoxiang <lihaoxiang9@huawei.com> - 0.12.0-3
- usbredirect: listen on correct address

* Thu Jan 20 2022 yanglongkang <yanglongkang@huawei.com> - 0.12.0-2
- modify the %files in spec file

* Tue Nov 23 2021 yanglongkang <yanglongkang@huawei.com> - 0.12.0-1
- update to 0.12.0

* Thu Sep 23 2021 yanglongkang <yanglongkang@huawei.com> - 0.8.0-7
- fix CVE-2021-3700

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.8.0-6
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Tue Jun 29 2021 zhouwenpei <zhouwenpei1@huawei.com> - 0.8.0-5
- add buildrequire gcc.

* Wed Sep 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.8.0-4
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESC:remove debuginfo

* Wed Aug 28 2019 zhanghaibo <ted.zhang@huawei.com> - 0.8.0-3
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESCi:openEuler Debranding

* Mon Aug 12 2019 zhanghaibo <ted.zhang@huawei.com> - 0.8.0-2
- Package init
